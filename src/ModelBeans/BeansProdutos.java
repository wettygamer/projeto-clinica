package ModelBeans;

public class BeansProdutos {

    public BeansProdutos(int cod_produto, String name_Produto) {
        this.cod_produto = cod_produto;
        this.name_Produto = name_Produto;
    }
    private int cod_produto;
    private String name_Produto;

    public int getCod_produto() {
        return cod_produto;
    }

    public void setCod_produto(int cod_produto) {
        this.cod_produto = cod_produto;
    }

    public String getName_Produto() {
        return name_Produto;
    }

    public void setName_Produto(String name_Produto) {
        this.name_Produto = name_Produto;
    }
    
    
}