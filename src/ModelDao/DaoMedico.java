package ModelDao;

import Control.ConexaoBD;
import ModelBeans.MedicoBeans;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class DaoMedico {
    ConexaoBD conex = new ConexaoBD();
    MedicoBeans mod = new MedicoBeans();
    
    public void Salvar (MedicoBeans mod) {
        conex.conexao();
        try {
            PreparedStatement pst = conex.con.prepareStatement("INSERT INTO medicos (nome_medico, especialidade, crm_medico) VALUES(?,?,?)");
            pst.setString(1, mod.getNome());
            pst.setString(2, mod.getEspecialidade());
            pst.setInt(3, mod.getCrm());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir dados:\n" + ex);
            System.out.println(ex);
        }
        conex.desconecta();
    }
    
    public MedicoBeans BuscarMedico (MedicoBeans mod) {
        conex.conexao();
        conex.executaSql("SELECT * FROM medicos where nome_medico like '%" + mod.getPesquisa() +"%'");
        try {
            conex.rs.first();
            mod.setCod(conex.rs.getInt("cod_medico"));
            mod.setNome(conex.rs.getString("nome_medico"));
            mod.setCrm(conex.rs.getInt("crm_medico"));
            mod.setEspecialidade(conex.rs.getString("especialidade"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro:\n" + ex);
        }
        conex.desconecta();
        
        return mod;        
    }
    
    public void Editar (MedicoBeans mod) {
        conex.conexao();
        try {
            PreparedStatement pst = conex.con.prepareStatement("UPDATE medicos SET nome_medico=?, especialidade=?, crm_medico=? where cod_medico=?");
            pst.setString(1, mod.getNome());
            pst.setString(2, mod.getEspecialidade());
            pst.setInt(3, mod.getCrm());
            pst.setInt(4, mod.getCod());
            System.out.println("Editado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao editar:\n" + ex);
        }
        
        conex.desconecta();
    }
    
}
