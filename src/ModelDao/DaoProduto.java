package ModelDao;

import Control.ConexaoBD;
import ModelBeans.BeansProdutos;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class DaoProduto {
    ConexaoBD connect = new ConexaoBD();
    //BeansProdutos prod = new BeansProdutos();
    
    public List<BeansProdutos> ListarProd () {
        List<BeansProdutos> listProd = new ArrayList();
        
        try {
            connect.conexao();
            connect.executaSql("SELECT * FROM produtos");
            while (connect.rs.next()){
                BeansProdutos prod = new BeansProdutos(connect.rs.getInt(1),connect.rs.getString(2));  
                listProd.add(prod);             
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error ao listar:\n"+ex);
        } finally {
            connect.desconecta();
        }
        return listProd;
    }
}
